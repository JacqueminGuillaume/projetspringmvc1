<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
        <base href="<%=request.getScheme()+"://"+request.getServerName()+":" + request.getServerPort()+request.getContextPath()+"/" %>" />
        <title><sitemesh:write property='title'>SpringMVC</sitemesh:write></title>
        <meta charset="utf-8" />
</head>
<body>
        <h1>Formation Spring MVC</h1>
        <hr />
        <sitemesh:write property="body" />
        <hr />
        <p>Ceci est le pied de page</p>
</body>
</html>
