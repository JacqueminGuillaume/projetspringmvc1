<%@ page language="java" contentType="text/html; charset=UTF-8" %>

<%-- intégration des balises JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- bibliothèques de balises Spring --%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE html>
<html>
<head>
	<base href="<%=request.getScheme()+"://"+request.getServerName()+":" + request.getServerPort()+request.getContextPath()+"/" %>" />
	<meta charset="utf-8">
	<title>Contactez-Nous</title>
</head>
<body>
	<h1>Contactez-nous</h1>
	<hr/>
	     <div style="color:red;">
                <spring:hasBindErrors name="login-form">
                        <c:forEach var="err" items="${errors.allErrors}">
                                <c:out value="${err.field}" />
                                <c:out value="${err.defaultMessage}" />
                        </c:forEach>
                </spring:hasBindErrors>
        </div>
	<form:form method="post" action="send" 
			   modelAttribute="contact-form">
		<form:label path="nom">Votre nom :</form:label>
		<form:input path="nom"/>
		<br/>	   
		<form:label path="email">email :</form:label>
		<form:input path="email"/>
		<br/>
		<form:label path="type">type de demande :</form:label>
		<form:select path="type">
			<form:options value="${typesList}"/>
		</form:select>	  
		<br/>
		
		<form:label path="message">message :</form:label>
		<form:textarea path="message"/> 
		<br/>
		
		<input type="submit" value="envoyer" />	   
	</form:form>
	<div>${msgRetour}</div>

</body>
</html>