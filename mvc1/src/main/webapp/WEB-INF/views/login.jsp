<%@ page language="java" contentType="text/html; charset=UTF-8" %>

<%-- intégration des balises JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- bibliothèques de balises Spring --%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE html>
<html>
<head>
	<base href="<%=request.getScheme()+"://"+request.getServerName()+":" + request.getServerPort()+request.getContextPath()+"/" %>" />
	
	<title><spring:message code="login.titleAuthentification" /></title>
	<meta charset="utf-8" />
</head>
<body>
	<h1><spring:message code="login.titleAuthentification" /></h1>
	
	    <a href="${requestScope['javax.servlet.forward.request_uri']}?lang=fr">FRANCAIS</a>
        <a href="${requestScope['javax.servlet.forward.request_uri']}?lang=en">ENGLISH</a>
	
		<div style="color:red;">
			<spring:hasBindErrors name="login-form">
				<c:forEach var="err" items="${errors.allErrors}">
					<c:out value="${err.field }"></c:out>
					<c:out value="${err.defaultMessage}"></c:out>
				</c:forEach>
			</spring:hasBindErrors>
		</div>
			
	<form:form method="post" action="check-login" 
			   modelAttribute="login-form">
			   
		<form:label path="email">
			<spring:message code="login.lblEmail"></spring:message>
		</form:label>  
		<form:input path="email" />
		<br />
		<form:label path="password">
			<spring:message code="login.lblPassword"></spring:message>
		</form:label>  
		<form:password path="password"/>
		<br />
		<input type="submit" value="<spring:message code="login.valueSeconnecter" />" />	   
	</form:form>
	<div>${msg}</div>
	
	
</body>
</html>














