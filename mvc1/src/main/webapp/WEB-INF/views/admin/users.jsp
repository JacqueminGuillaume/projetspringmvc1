<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!doctype html>
<html>
<head>
<!--  la balise base sert à préfixer l'ensemble
        des liens et des images pour éviter des
        problèmes de redirection -->
<base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/"%>" />
<title>Liste des utilisateurs</title>
<meta charset="utf-8" />
</head>
<body>
	<h1>Liste des utilisateurs</h1>
	<a href="admin/add-user">Ajouter un utilisateur</a>

	<fieldset>
		<legend>Recherche par nom :</legend>
		<form:form method="post" action="admin/search-user" modelAttribute="u">
			<form:input path="name" />
			<input type="submit" value="rechercher" />
		</form:form>

	</fieldset>
	<c:choose>
		<c:when test="${resVide}">
			<p>Aucun resultat !</p>
		</c:when>
		<c:otherwise>

			<table border="1">
				<tr>
					<th>Nom</th>
					<th>Email</th>

					<th>Admin ?</th>
					<th>Actions</th>
				</tr>
				<c:forEach var="u" items="${ users }">
					<tr>
						<td>${ u.name }</td>
						<td>${ u.email }</td>

						<td><c:choose>
								<c:when test="${ u.admin }">Oui</c:when>
								<c:otherwise>Non</c:otherwise>
							</c:choose></td>
						<td><a href="admin/users/update/${ u.id }" title="Modifier">Modifier</a>
							<a href="admin/users/delete/${ u.id }" title="Supprimer">Supprimer</a>
						</td>
					</tr>
				</c:forEach>
			</table>

			<div>
				<c:if test="${page>1 }">
					<a href="admin/users?page=${page-1}&max=${max}">Precedent</a>
				</c:if>
				<span>${page }</span>
				<c:if test="${suivExist}">
					<a href="admin/users?page=${page+1}&max=${max}">Suivant</a>
				</c:if>
			</div>

		</c:otherwise>
	</c:choose>


	<a href="admin/export-users" title="Exporter">Export CSV </a>

	<h4>Import Csv</h4>
	<form:form method="post" action="admin/upload-users"
		enctype="multipart/form-data">
		<input type="file" name="file" />
		<br />
		<input type="submit" value="Uploader" />

	</form:form>

</body>
</html>
