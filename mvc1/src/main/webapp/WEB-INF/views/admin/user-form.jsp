<%@ page language="java" contentType="text/html; charset=UTF-8" %>

<%-- intégration des balises JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- bibliothèques de balises Spring --%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE html>
<html>
<head>
	<base href="<%=request.getScheme()+"://"+request.getServerName()+":" + request.getServerPort()+request.getContextPath()+"/" %>" />
	
	<title>${title}</title>
	<meta charset="utf-8" />
</head>
<body>
	<h1>${title}</h1>
	
	<form:form method="post" action="admin/save-user" 
			   modelAttribute="u">
			   
		<form:label path="name">Nom :</form:label>  
		<form:input path="name" />
		<br />
		<form:label path="email">Email :</form:label>  
		<form:input path="email" />
		<br />
		<!-- en modification, on ne peut pas changer le mot de passe -->
		<c:choose>
			<c:when test="${isAdd}">
		<form:label path="password">Mot de passe :</form:label>  
		<form:password path="password" showPassword="true"/>
		<br />
			</c:when>
			<c:otherwise>
				<form:hidden path="password"/>
			</c:otherwise>
		</c:choose>
		<form:label path="admin">Admin? :</form:label>  
		<form:checkbox path="admin"/>
		<br />
		<form:hidden path="id"/>
		<form:hidden path="version"/>
		
		<form:label path="currentStatus">Statut :</form:label>  
		<form:input path="currentStatus" />
		<br />
		
		
		<input type="submit" value="Sauvegarder" />	   
	</form:form>
	<div>${msg}</div>
	
	
</body>
</html>














