package tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.dawan.mvc1.beans.User;

/** 
 * 
 * */
public class Tools {
	
	/** 
	 * Import d'un fichier csv
	 * @param filePath chemin complet vers le fichier
	 * @return liste d'objets User
	 * @throws Exception si erreur d'import
	 */
	public static List<User>  importCsv(String filePath) throws Exception {
		List<User> users = new ArrayList<>();
		try(BufferedReader reader = new BufferedReader(new FileReader(filePath))){
			
			reader.readLine();//ligne d'entete
			String ligne = null;
			while ( (ligne = reader.readLine()) != null) {
				
				if (!ligne.trim().isEmpty()) { //si ligne non vide
					String[] ligneDecoupe = ligne.split(";"); //decouper la ligne par rapport au ; (split)
					if (ligneDecoupe.length == 3 ) { //si 3 éléments dans le tableau
						User u = new User(ligneDecoupe[0], ligneDecoupe[1], Boolean.parseBoolean(ligneDecoupe[2]));  //créer un User et remplir : name,email, admin
						users.add(u);  //ajouter l'utilisateur créé à la liste
					}
				}
	
			}
			
		}
		
		
		return users;
		
		
	}

	// methode qui exporte en csv une liste d'utilisateurs
	// visibilite mot clef typeRetour nomMethode(parametre)

	// méthode qui exporte en csv une liste d'utilisateurs
	// visibilité mots-clé typeRetour nomMethode(paramètres)
	public static <T> void toCsv(String filePath, List<T> myList, List<String> columns, String separator)
			throws Exception {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath))) {
			if (myList != null && myList.size() > 0) {
				Field[] tab = myList.get(0).getClass().getDeclaredFields();
				StringBuilder ligneEntete = new StringBuilder();
				for (Field f : tab) {
					f.setAccessible(true);
					if (columns.contains(f.getName()))
						ligneEntete.append(f.getName()).append(separator);
				}
				bw.write(ligneEntete.toString().substring(0, ligneEntete.length() - 1));
				bw.newLine();
				for (T obj : myList) {
					StringBuilder line = new StringBuilder();
					for (Field f : tab) {
						if (columns.contains(f.getName()))
							line.append(f.get(obj).toString()).append(separator);
					}
					bw.write(line.toString().substring(0, line.length() - 1));
					bw.newLine();
				}
			}
		}
	}
	


	public static void main(String[] args) {
		List<User> lu = new ArrayList<>();
		for (int i = 1; i < 10; i++) {
			lu.add(new User(new Long(i), "name" + i, "email" + i, "password" + i));
		}
		List<String> columns = Arrays.asList("name", "email");
		try {
			Tools.toCsv("users.csv", lu, columns, ";");
			System.out.println("export effectué");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
