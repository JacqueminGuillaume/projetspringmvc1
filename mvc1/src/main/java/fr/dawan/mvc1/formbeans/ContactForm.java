package fr.dawan.mvc1.formbeans;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class ContactForm {
	@NotEmpty
	private String nom;
	
	@NotEmpty
	@Pattern(regexp="\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b", message="Email invalide")
	private String email;
	
	
	public enum DemandType{
		RECLAMATION, QUESTION, AUTRE
	}
	@NotEmpty
	private DemandType type;
	
	@NotEmpty
	private String message;

	
	
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public DemandType getType() {
		return type;
	}

	public void setType(DemandType type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	

	


	
	
	
	

}
