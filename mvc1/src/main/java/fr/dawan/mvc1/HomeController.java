package fr.dawan.mvc1;

import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.dawan.mvc1.beans.User;
import fr.dawan.mvc1.beans.User.UserStatus;
import fr.dawan.mvc1.dao.UserDao;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Autowired
	private UserDao userDao;
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		
		return "home";
	}
	
	@GetMapping("/test/insert-data")
	public String insertData() {
		for (int i = 0; i < 50; i++) {
			User u = new User(null, "toto"+i, "toto"+i+"@dawan.fr", "toto"+i);
			u.setCreationDate(new Date());
			u.setAdmin(false);
			u.setCurrentStatus(UserStatus.ACTIVE);
			userDao.insert(u);
			userDao.getHibernateTemplate().evict(u);
			
		}
		return "home";
	}
	
	public UserDao getUserDao() {
		return userDao;
	}
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
}
