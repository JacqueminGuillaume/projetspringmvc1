package fr.dawan.mvc1.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.mvc1.beans.User;

public class UserDao {
	
	private HibernateTemplate hibernateTemplate;

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
		
	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}


	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<User> findAll() {		
	return (List<User>)hibernateTemplate.find("FROM User" ,null);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<User> findAll(int start, int nb) {		
		return hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("FROM User").setFirstResult(start).setMaxResults(nb).list();
	}
	
	@Transactional(readOnly=true)
	public long nbUsers() {
		return (long) hibernateTemplate.find("SELECT COUNT (u.id) FROM User u", null).get(0);
	}

	@Transactional
	public Long insert(User u) {
		return (Long) hibernateTemplate.save(u);
	}
	@Transactional(readOnly=true)
	public User findById(long id) {
		
		return hibernateTemplate.get(User.class, id);
	}
	@Transactional
	public  void update(User u) {
		hibernateTemplate.saveOrUpdate(u);
	}
	@Transactional
	public  void remove(long id) {
		hibernateTemplate.delete(findById(id));
	}
	@Transactional(readOnly=true)
	public  List<User> findByName(String rech){
		return hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("FROM User u WHERE u.name LIKE :rech").setParameter("rech", "%"+rech+"%").list();
		
	}
	@Transactional(readOnly=true)
	public  User findByEmail(String email){
		return (User) hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("FROM User u WHERE u.email LIKE :email").setParameter("email", "%"+email+"%").list().get(0);
		
	}
	
	

}
