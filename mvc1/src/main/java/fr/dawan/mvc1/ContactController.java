package fr.dawan.mvc1;

import javax.validation.Valid;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import fr.dawan.mvc1.formbeans.ContactForm;
import fr.dawan.mvc1.formbeans.ContactForm.DemandType;

@Controller
public class ContactController {
	
	@GetMapping("/contact")
	public String showContact(Model model) {
		//TODO passer un formBeans dans le model
		model.addAttribute("contact-form", new ContactForm());		
		model.addAttribute("typesList", DemandType.values());
		return "contact";
	}
	
	@PostMapping("/send")
	public String sendEmail(Model model, @Valid @ModelAttribute("contact-form") ContactForm form, BindingResult result) {
		
		try {
			Email email = new SimpleEmail();
			email.setHostName("smtp.googlemail.com");
			email.setSmtpPort(465);
			email.setAuthenticator(new DefaultAuthenticator("username", "password"));
			email.setSSLOnConnect(true);
			email.setFrom(form.getEmail());
			email.setSubject(form.getType().toString());
			email.setMsg(form.getMessage());
			email.addTo("jacquemin.g59@hotmail.fr");
			email.send();
			model.addAttribute("msgRetour", "message envoyé");
				
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("msgRetour", "Erreur : " + e.getMessage());
			model.addAttribute("contact-form", form);
		}
		
		
		return "contact";
		
	}
	

}
